module View.Attributes exposing (..)


type alias Attribute c =
    c -> c

type Shape 
    = Rbox Int Int
    | Circle Int 
    | Ellipse Int Int 


type alias StyStr = 
    {
        size : String
        , weight : String
        , family : String
    }

type alias BgColor = 
    {
        displayTop : String
        , displayBottom : String
        , body : String
    }
{- Function to set the width attribute of CalcConfig -}


width : Float -> Attribute { c | width : Float }
width f =
    \cc ->
        { cc | width = f }



{- Function to set the height attribute of CalcConfig -}

height : Float -> Attribute { c | height : Float }
height f =
    \cc ->
        { cc | height = f }


shape: Shape -> Attribute { c | shape : Shape}
shape f = 
    \cc ->
        { cc | shape = f}

dispcolor: BgColor -> Attribute { c | dispcolor : BgColor}
dispcolor f = 
    \cc ->
        { cc | dispcolor = f}

fcolor: String -> Attribute { c | fcolor : String}
fcolor f = 
    \cc ->
        { cc | fcolor = f}

strSty : StyStr -> Attribute { c | strSty : StyStr}
strSty f = 
    \cc ->
        { cc | strSty = f}
columns : Int -> Attribute { c | columns : Int}
columns f =
    \cc ->
        { cc | columns = f}

padding : String -> Attribute { c | padding : String}
padding f = 
    \cc ->
        { cc | padding = f}

btnDims : (Int,Int) -> Attribute { c | btnDims : (Int,Int)}
btnDims f =
    \cc ->
        { cc | btnDims = f}    